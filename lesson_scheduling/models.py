from django.db import models


class Person(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(null=True)

    def getName(self):
        return "{0} {1}".format(self.first_name, self.last_name)


class Student(Person):
    teacher = models.ManyToManyField("Teacher")

    def __str__(self):
        return "Student: {0}".format(self.getName())


class Teacher(Person):
    def __str__(self):
        return "Teacher: {0}".format(self.getName())


class ChamberGroup(models.Model):
    name = models.CharField(max_length=50)
    student = models.ManyToManyField("Student")
    coach = models.ManyToManyField("Teacher")

    def __str__(self):
        return "ChamberGroup: {}".format(self.name)


class Room(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return "Room: {}".format(self.name)


class Lesson(models.Model):
    teacher = models.ForeignKey("Teacher")
    room = models.ForeignKey("Room", null=True)
    date = models.DateField()
    start_time = models.TimeField()
    duration = models.SmallIntegerField(default=60)

    def __str__(self):
        return "Teacher: {}, at {},{} duration: {}"\
            .format(self.teacher.getName(), self.date, self.start_time,
                    self.duration)


class SingleStudentLesson(Lesson):
    student = models.ForeignKey("Student", null=True)


class ChamberGroupLesson(Lesson):
    chamber_group = models.ForeignKey("ChamberGroup", null=True)

