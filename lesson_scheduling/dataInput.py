import csv
import os
import re
import datetime as dt

from .timeline import Time


def readFile(filename, delimiter):
    abs_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            filename)
    with open(abs_path) as csvfile:
        mreader = csv.reader(csvfile, delimiter=delimiter)
        rows = [row for row in mreader]
        return rows


def readTsv(filename):
    return readFile(filename, "\t")


def readCsv(filename):
    return readFile(filename, ",")


def saveTeachersFromSheet():
    from .modelUtil import saveTeacher, getStudent, addTeacherStudent
    rows = readTsv('student_info.tsv')
    for row in rows[1:]:
        # get student for teacher-student relation
        s_first_name, s_last_name = readStudentNameFromSheet(row)
        student = getStudent(s_first_name, s_last_name)
        for i in range(3, 6):  # column 8, 9, 10 are teachers.
            name_str = row[i].strip()
            if not name_str:  # ignore empty string
                continue
            t_first_name, t_last_name = parseNameWithComma(name_str)
            t = saveTeacher(t_first_name, t_last_name)
            addTeacherStudent(t, student)


def ignoreParentheses(s):
    return re.sub(r"\(.*\)", "", s).strip()


def parseNameWithComma(name_str):
    """Return a tuple of first and last name
       parsed from a single 'last, first' string."""
    if "," in name_str:
        last_name, first_name = name_str.split(",", 1)
    else:
        last_name, first_name = name_str, ""
    return (first_name.strip(), last_name.strip())


def parseNameWithBlank(name_str):
    """Return a tuple of first and last name
       parsed from a single 'first last' string."""

    name_str = name_str.strip()
    if " " in name_str:
        first_name, last_name = name_str.rsplit(" ", 1)
    else:
        first_name, last_name = "", name_str
    return (first_name.strip(), last_name.strip())


def saveStudentsFromSheet():
    from .modelUtil import saveStudent
    rows = readTsv('student_info.tsv')
    for row in rows[1:]:
        first_name, last_name = readStudentNameFromSheet(row)
        email = row[6].strip()
        saveStudent(first_name, last_name, email)


def readStudentNameFromSheet(row):
    first_name = row[0].strip()
    # if first_name[:4] == "ZZZZ":
    #     continue  # ignore students on a leave.
    last_name = row[1].strip()
    return first_name, last_name


def saveStudentScheduleFromSheet():
    from .modelUtil import getCollegeTeacher, getStudent,\
        saveSingleStudentLesson
    teacher = getCollegeTeacher()
    rows = readCsv('student_schedule.csv')
    for row in rows[1:]:
        if (not row[15]) or (not row[16]):
            continue
        hh, mm = divmod(int(row[16]), 100)
        end_time = Time(hh, mm)
        hh, mm = divmod(int(row[15]), 100)
        duration = (end_time - Time(hh, mm)).minute
        start_time = dt.time(hh, mm)

        last_name, first_name = row[1].strip(), row[2].strip()
        student = getStudent(first_name, last_name)
        if not student:
            continue

        yyyy, mm, dd = [int(x) for x in row[13].split("/")]
        start_date = dt.date(yyyy, mm, dd)
        yyyy, mm, dd = [int(x) for x in row[14].split("/")]
        end_date = dt.date(yyyy, mm, dd)

        # won't add lessons before today
        today = dt.date.today()
        if start_date < today:
            d = today
        else:
            d = start_date

        while d <= end_date:
            dayOfWeek = d.weekday()
            if dayOfWeek < 5 and row[6+dayOfWeek].strip():
                saveSingleStudentLesson(teacher, student, d,
                                        start_time, duration)
            d += dt.timedelta(1)  # add 1 day


def saveChamberGroupFromSheet():
    from .modelUtil import saveChamberGroup, saveTeacher,\
        addChamberGroupCoach, getStudent, addChamberGroupMember
    rows = readCsv('chamber_groups.csv')
    for row in rows[1:]:
        c = saveChamberGroup(row[0].strip())
        # teachers
        for i in range(1, 4):
            name_str = row[i].strip()
            if not name_str:
                continue
            t_first_name, t_last_name = parseNameWithBlank(name_str)
            t = saveTeacher(t_first_name, t_last_name)
            addChamberGroupCoach(c, t)
        # students
        for i in range(4, len(row)):
            name_str = row[i].strip()
            if not name_str:
                continue
            s_first_name, s_last_name = parseNameWithBlank(row[i])
            s = getStudent(s_first_name, s_last_name)
            if s:
                addChamberGroupMember(c, s)
