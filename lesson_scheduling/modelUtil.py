from django.shortcuts import get_object_or_404

from .models import Teacher, Student, ChamberGroup, \
  SingleStudentLesson, ChamberGroupLesson
from .timeline import Calendar, \
  commonCalendar

COLLEGE_TEACHER_NAME = "-"

class TeacherUtil(object):
  @staticmethod
  def getTeachers():
    # Get the list of all teachers.
    return Teacher.objects.order_by('last_name')

  @staticmethod
  def getTeacher(first_name, last_name):
    # Get the Teacher object by name.
    # Returns None when there's no such teacher.
    try:
      return Teacher.objects.get(first_name=first_name,
                                 last_name=last_name)
    except Teacher.DoesNotExist:
      # TODO: decide whether to deal with MultipleObjectsReturned
      return None

  @staticmethod
  def getTeacherById(teacher_id):
    # Get the Teacher object by id.
    # Returns None when there's no such teacher.
    try:
      return Teacher.objects.get(pk=teacher_id)
    except Teacher.DoesNotExist:
      return None

  @staticmethod
  def saveTeacher(first_name, last_name):
    return Teacher.objects.get_or_create(first_name=first_name,
                       last_name=last_name)[0]

  @staticmethod
  def getCollegeTeacher():
    return Teacher.objects.get_or_create(first_name=COLLEGE_TEACHER_NAME,
                       last_name=COLLEGE_TEACHER_NAME)[0]

def getTeacher(first_name, last_name):
  print "modelUtil.getTeacher deprecated"
  return TeacherUtil.getTeacher(first_name, last_name)

def getTeacherFromId(teacher_id):
  print "modelUtil.getTeacherFromId deprecated"
  return TeacherUtil.getTeacherById(teacher_id)

def saveTeacher(first_name, last_name):
  return Teacher.objects.get_or_create(first_name=first_name,
                     last_name=last_name)[0]

def getCollegeTeacher():
  return Teacher.objects.get_or_create(
    first_name=COLLEGE_TEACHER_NAME,
    last_name=COLLEGE_TEACHER_NAME)[0]



def getStudent(first_name, last_name):
  try:
    return Student.objects.get(first_name=first_name,
                   last_name=last_name)
  except Student.DoesNotExist:
    return None


def getStudentFromId(student_id):
  try:
    return Student.objects.get(pk=student_id)
  except Student.DoesNotExist:
    return None


def saveStudent(first_name, last_name, email):
  return Student.objects.get_or_create(first_name=first_name,
                     last_name=last_name,
                     email=email)[0]


def getStudentsOfTeacher(teacher_id):
  teacher = getTeacherFromId(teacher_id)
  return teacher.student_set.all()


def addTeacherStudent(teacher, student):
  student.teacher.add(teacher)


def getChamberGroupOfTeacher(teacher_id):
  teacher = getTeacherFromId(teacher_id)
  return teacher.chambergroup_set.all()


def getStudentLessons(student_id, date):
  # Single student lessons
  student = getStudentFromId(student_id)
  lessons = student.singlestudentlesson_set.filter(date=date)
  L = list(lessons)  # convert QuerySet object into list
  # Chamber group lessons
  chamber_groups = student.chambergroup_set.all()
  for chamber_group in chamber_groups:
    chamber_lessons = chamber_group.chambergrouplesson_set.filter(
      date=date)
    L.extend(list(chamber_lessons))
  return L


def getCollegeClass():
  # Get the empty teacher for all college classes
  teacher = getCollegeTeacher()
  return teacher.lesson_set.all()


def saveChamberGroup(name):
  return ChamberGroup.objects.get_or_create(name=name)[0]


def addChamberGroupCoach(group, teacher):
  group.coach.add(teacher)


def addChamberGroupMember(group, student):
  group.student.add(student)


def getChamberGroupFromId(chamber_group_id):
  try:
    return ChamberGroup.objects.get(pk=chamber_group_id)
  except ChamberGroup.DoesNotExist:
    return None


def saveSingleStudentLesson(teacher, student, date, start_time, duration):
  return SingleStudentLesson.objects.get_or_create(
    teacher=teacher, student=student, date=date,
    start_time=start_time, duration=duration)[0]


def getStudentCalendar(student_id, date):
  # TODO
  L = getStudentLessons(student_id, date)
  calendar = Calendar()
  for l in L:
    calendar.addLesson(l)
  return calendar


def getStudentCalendarMultiDays(student_id, dates):
  calendar = Calendar()
  for date in dates:
    L = getStudentLessons(student_id, date)
    for l in L:
      calendar.addLesson(l)
  return calendar


def getChamberGroupCalendar(chamber_group_id, date):
  # TODO
  chamber_group = get_object_or_404(ChamberGroup, pk=chamber_group_id)
  students = chamber_group.student.all()
  calendar_list = [getStudentCalendar(s.id, date) for s in students]
  return commonCalendar(calendar_list)


def getChamberGroupCalendarMultiDays(chamber_group_id, dates):
  # TODO
  chamber_group = get_object_or_404(ChamberGroup, pk=chamber_group_id)
  students = chamber_group.student.all()
  calendar_list = [getStudentCalendarMultiDays(s.id, dates) for s in students]
  return commonCalendar(calendar_list)
