class Time(object):
    def __init__(self, hour=0, minute=0):
        assert 0 <= hour < 24
        self.hour = int(hour)
        assert 0 <= minute < 60
        self.minute = int(minute)

    def fromString(self, string):
        hour = int(string[:2])
        minute = int(string[3:])
        assert 0 <= hour < 24
        self.hour = int(hour)
        assert 0 <= minute < 60
        self.minute = int(minute)
        return self

    def __str__(self):
        return "{0:02d}:{1:02d}".format(self.hour, self.minute)

    def __repr__(self):
        return self.__str__()

    def __sub__(self, other):
        assert isinstance(other, Time)
        return Period(self.inMinutes()-other.inMinutes())

    def inMinutes(self):
        return self.hour * 60 + self.minute

    def fromMinutes(self, n):
        n = n % (24*60)
        self.hour, self.minute = divmod(n, 60)

    def plusPeriod(self, p):
        assert isinstance(p, Period)
        n = self.inMinutes() + p.minute
        self.fromMinutes(n)

    def plusPeriodNew(self, p):
        t = Time(self.hour, self.minute)
        t.plusPeriod(p)
        return t


class Period(object):
    def __init__(self, minute):
        self.minute = int(minute)

    def __str__(self):
        return "Period: {0} minutes".format(self.min)

    def __div__(self, other):
        assert isinstance(other, Period)
        return self.minute // other.minute

    def __mul__(self, n):
        assert isinstance(n, int)
        return Period(self.minute*n)


class Calendar(object):

    DAY_START = Time(8, 30)
    DAY_END = Time(22, 5)
    GRANULARITY = Period(5)
    EMPTY = 0
    OCCUPIED = 1
    DISPLAY = {EMPTY: "...\n", OCCUPIED: "XX\n"}

    def __init__(self):
        self.timeline = [Calendar.EMPTY] * \
            ((Calendar.DAY_END - Calendar.DAY_START) / Calendar.GRANULARITY)

    def findIndex(self, t):
        # assert isinstance(t, Time)
        return (t - Calendar.DAY_START) / Calendar.GRANULARITY

    def findSpan(self, duration_in_min):
        """Find the corresponding time span in timeline"""
        duration = Period(duration_in_min)
        return duration / Calendar.GRANULARITY

    def indexToTime(self, i):
        return Calendar.DAY_START.plusPeriodNew(Calendar.GRANULARITY * i)

    def addLesson(self, lesson):
        start_time = parseLessonStartTime(lesson)
        start_index = self.findIndex(start_time)
        span = self.findSpan(lesson.duration)
        for i in range(start_index, start_index+span):
            self.timeline[i] = Calendar.OCCUPIED

    def proposeTime(self, duration_in_min):
        # duration_in_min is an int
        span = self.findSpan(duration_in_min)
        possible_index = []
        for i in range(len(self.timeline) - span + 1):
            if max(self.timeline[i:i+span]) == Calendar.EMPTY:
                possible_index.append(i)
        return [self.indexToTime(idx) for idx in possible_index]

    def checkTimeAvailable(self, start_time, duration): # Time and int
        i = self.findIndex(start_time)
        span = self.findSpan(duration)
        timeline = self.timeline
        for idx in range(i, i+span):
            if timeline[idx] == Calendar.OCCUPIED:
                return False
        return True

    def showTimeline(self):
        return "<br>".join([Calendar.DISPLAY[p] for p in self.timeline])


def parseLessonStartTime(lesson):
    start = lesson.start_time
    return Time(start.hour, start.minute)


def commonTimeline(calendar_list):
    timeline_list = [c.timeline for c in calendar_list]
    num_cal = len(timeline_list)
    num_time_slot = len(timeline_list[0])
    common_timeline = [Calendar.EMPTY] * num_time_slot
    for i in range(num_time_slot):
        for j in range(num_cal):
            if (timeline_list[j][i] == Calendar.OCCUPIED):
                common_timeline[i] = Calendar.OCCUPIED
                break
    return common_timeline


def commonCalendar(calendar_list):
    c = Calendar()
    c.timeline = commonTimeline(calendar_list)
    return c




