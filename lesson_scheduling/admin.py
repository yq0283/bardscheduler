from django.contrib import admin

import models

# Register your models here.
admin.site.register(models.Student)
admin.site.register(models.Teacher)
admin.site.register(models.ChamberGroup)
admin.site.register(models.Room)
admin.site.register(models.SingleStudentLesson)
admin.site.register(models.ChamberGroupLesson)
