from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.views import generic
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from .models import Teacher, Student, \
  SingleStudentLesson, ChamberGroupLesson
from .timeline import Period, Time
from .scheduleUtil import readScheduleForm, scheduleClasses
from .modelUtil import TeacherUtil

import datetime as dt


class IndexView(generic.ListView):
    """Show lists of teachers as the starting point of workflow.
    If there's no teacher, display a link to read the input data from csv files.
    Clicking on a teacher's name brings the user to the teacher view."""
    template_name = 'lesson_scheduling/index.html'
    context_object_name = 'teacher_list'

    def get_queryset(self):
        return TeacherUtil.getTeachers()


# TODO: Resolve CSRF issue.
@csrf_exempt
def teacher(request, teacher_id):
  from .modelUtil import getStudentsOfTeacher, getChamberGroupOfTeacher
  mTeacher = get_object_or_404(Teacher, pk=teacher_id)
  student_list = getStudentsOfTeacher(teacher_id)
  chamber_group_list = getChamberGroupOfTeacher(teacher_id)

  template = loader.get_template('lesson_scheduling/teacher.html')
  context = RequestContext(request, {
    "teacher": mTeacher,
    "student_list": student_list,
    "chamber_group_list": chamber_group_list,
  })
  return HttpResponse(template.render(context))

def students(request):
  student_list = Student.objects.order_by('last_name')
  template = loader.get_template('lesson_scheduling/students.html')
  context = RequestContext(request, {
    'student_list': student_list,
  })
  return HttpResponse(template.render(context))


def chambergroups(request):
  from .models import ChamberGroup
  group_list = ChamberGroup.objects.order_by('name')
  template = loader.get_template('lesson_scheduling/chamber_groups.html')
  context = RequestContext(request, {
    'group_list': group_list,
  })
  return HttpResponse(template.render(context))


def chambergroup(request, chamber_group_id):
  from .models import ChamberGroup
  mChamberGroup = get_object_or_404(ChamberGroup, pk=chamber_group_id)
  student_list = mChamberGroup.student.all()

  template = loader.get_template('lesson_scheduling/chamber_group.html')
  context = RequestContext(request, {
    'chamber_group': mChamberGroup,
    'student_list': student_list,
  })
  return HttpResponse(template.render(context))


def student(request, student_id):
  from .modelUtil import getStudentLessons
  from .timeline import parseLessonStartTime

  mStudent = get_object_or_404(Student, pk=student_id)
  name = mStudent.getName()
  date = dt.date.today()  # TODO: enable date
  lessons = getStudentLessons(student_id, date)
  L = []
  for lesson in lessons:
    start = parseLessonStartTime(lesson)
    end = start.plusPeriodNew(Period(lesson.duration))
    teacher_name = lesson.teacher.getName()
    L.append((teacher_name, start, end))
  template = loader.get_template('lesson_scheduling/student.html')
  context = RequestContext(request, {
    'student_name': name,
    'lesson_list': L,
  })
  return HttpResponse(template.render(context))





@csrf_exempt
def schedule(request, teacher_id):
  input_date = request.POST['date']
  input_repeat = ('repeat' in request.POST)
  input_last_date = request.POST['last_date']

  request.session['date'] = input_date
  request.session['repeat'] = input_repeat
  request.session['last_date'] = input_last_date

  dates = getDates(input_date, input_repeat, input_last_date)

  start_time = parseTime(request.POST['start_time'])
  # get data from the form
  D, calendars, ranks = readScheduleForm(request, teacher_id, dates)
  schedule_list = scheduleClasses(D, ranks, start_time, calendars)
  mTeacher = get_object_or_404(Teacher, pk=teacher_id)

  template = loader.get_template('lesson_scheduling/chooseSchedule.html')
  context = RequestContext(request, {
    "teacher": mTeacher,
    "schedule_list": schedule_list,
    "teacher_id": teacher_id,
  })
  return HttpResponse(template.render(context))


def parseDate(date_str):
  L = date_str.split("/")
  assert len(L) == 3
  mm, dd, yyyy = [int(x) for x in L]
  return dt.date(yyyy, mm, dd)


def parseTime(time_str):  # same as Time().fromString()
  L = time_str.split(":")
  assert len(L) == 2
  hh, mm = [int(x) for x in L]
  return Time(hh, mm)

def getDates(date, repeat, last_date):
  date = parseDate(date)
  dates = [date]
  if repeat:
    # add the same day of each week before the last date to dates
    last_date = parseDate(last_date)
    timedelta_week = dt.timedelta(7)
    current_date = date + timedelta_week
    while current_date <= last_date:
      dates.append(current_date)
      current_date += timedelta_week
  return dates



@csrf_exempt
def saveSchedule(request, teacher_id):
  from .modelUtil import getStudentFromId, getChamberGroupFromId
  input_date = request.session['date']
  input_repeat = request.session['repeat']
  input_last_date = request.session['last_date']
  dates = getDates(input_date, input_repeat, input_last_date)

  schedule = (request.POST['choice'][5:-6]).split("<br/>")
  for date in dates:
    for lesson in schedule:
      saveLesson(lesson, teacher_id, date)

  # Format lessons
  formatted_schedule = []
  for lesson in schedule:
    time_str = lesson[31:]
    start_time = time_str[:5]
    end_time = time_str[10:]
    if lesson[1] == "S":
      student_id = int(lesson[2:6])
      student = getStudentFromId(student_id)
      name = '{} {}'.format(student.first_name, student.last_name)
    else:
      chamber_group_id = lesson[2:6]
      chamber_group = getChamberGroupFromId(chamber_group_id)
      name = chamber_group.name
    formatted_schedule.append("{} - {} {}".format(start_time, end_time, name))

  teacher = get_object_or_404(Teacher, pk=teacher_id)

  template = loader.get_template('lesson_scheduling/schedule_saved.html')
  context = RequestContext(request, {
    "dates": dates,
    "schedule": formatted_schedule,
    "teacher": teacher,
  })
  return HttpResponse(template.render(context))


def saveLesson(lesson_str, teacher_id, date):
  start_time, duration = find_time_duration(lesson_str[31:])
  if lesson_str[1] == "S":
    student_id = int(lesson_str[2:6])
    l = SingleStudentLesson(teacher_id=teacher_id, student_id=student_id,
                date=date, start_time=start_time,
                duration=duration)
    l.save()
  elif lesson_str[1] == "C":
    chamber_group_id = int(lesson_str[2:6])
    l = ChamberGroupLesson(teacher_id=teacher_id,
                 chamber_group_id=chamber_group_id,
                 date=date, start_time=start_time,
                 duration=duration)
    l.save()


def find_time_duration(lesson_time_str):
  t1_str = lesson_time_str[:5]
  t2_str = lesson_time_str[10:]
  t1 = Time().fromString(t1_str)
  t2 = Time().fromString(t2_str)
  duration = (t2-t1).minute
  start_time = dt.time(t1.hour, t1.minute)
  return start_time, duration


def dataInput(request):
  """Populate empty database with data."""
  from .dataInput import saveStudentsFromSheet, saveTeachersFromSheet,\
    saveChamberGroupFromSheet, saveStudentScheduleFromSheet
  saveStudentsFromSheet()
  saveTeachersFromSheet()
  saveChamberGroupFromSheet()
  saveStudentScheduleFromSheet()
  return redirect("/")


def updateStudentSchedule(request):
  """Update students' college class schedule."""
  from .dataInput import saveStudentScheduleFromSheet
  from .modelUtil import getCollegeClass
  getCollegeClass().delete()  # Delete all college classes
  saveStudentScheduleFromSheet()  # Add the new version
  return redirect("/")
