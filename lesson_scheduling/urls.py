from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    # Student
    url(r'^student/$', views.students, name="students"),
    url(r'^student/(?P<student_id>[0-9]+)/$',
        views.student, name="student"),
    # Teacher
    url(r'^teacher/(?P<teacher_id>[0-9]+)/$',
        views.teacher, name="teacher"),
    url(r'^teacher/(?P<teacher_id>[0-9]+)/schedule/$',
        views.schedule, name="schedule"),
    url(r'^teacher/(?P<teacher_id>[0-9]+)/schedule/save/$',
        views.saveSchedule, name="saveSchedule"),
    # Chamber Groups
    url(r'^chamber_groups/$', views.chambergroups, name="chamber_groups"),
    url(r'^chamber_group/(?P<chamber_group_id>[0-9]+)/$',
        views.chambergroup, name="chamber_group"),
    # Input and update
    url(r'input/$', views.dataInput, name="dataInput"),
    url(r'update/student_schedule/$',
        views.updateStudentSchedule, name="updateStudentSchedule"),
    ]
