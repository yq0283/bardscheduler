from .models import Teacher

def createTeacher(first_name, last_name, email=None):
    return Teacher.objects.create(first_name=first_name, last_name=last_name,
        email=email)
