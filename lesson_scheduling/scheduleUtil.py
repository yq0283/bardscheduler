from .modelUtil import getStudentsOfTeacher, getChamberGroupOfTeacher,\
    getStudentCalendar, getStudentCalendarMultiDays, getChamberGroupCalendar,\
    getChamberGroupCalendarMultiDays
from .timeline import Period
from .models import Student, ChamberGroup

from itertools import permutations
from django.shortcuts import get_object_or_404


def readScheduleForm(request, teacher_id, dates):
    durations, positions = {}, {}
    calendars = {}  # get all calendars

    for student in getStudentsOfTeacher(teacher_id):
        duration = int(request.POST['durationS'+str(student.id)])
        if duration == 0:
            continue
        position = int(request.POST['positionS'+str(student.id)])
        # prepare the dicts
        key = "S"+str(student.id)
        durations[key], positions[key] = duration, position
        calendars[key] = getStudentCalendarMultiDays(student.id, dates)

    for chamber_group in getChamberGroupOfTeacher(teacher_id):
        duration = int(request.POST['durationC'+str(chamber_group.id)])
        if duration == 0:
            continue
        position = int(request.POST['positionC'+str(chamber_group.id)])
        # prepare the dicts
        key = "C"+str(chamber_group.id)
        durations[key], positions[key] = duration, position
        calendars[key] = getChamberGroupCalendarMultiDays(chamber_group.id, dates)

    # break
    duration = int(request.POST['durationB'])
    position = int(request.POST['positionB'])
    if duration != 0:
        durations["B"] = duration
        positions["B"] = position
    return durations, calendars, positions


def scheduleClasses(D, positions, start_time, calendars):
    # D is the dict for class duration to be scheduled.
    l = findPermutations(positions)  # find all permutations

    S = []
    for sequence in l:
        # check each one for conflict
        classStartTimes = sequenceToClassStartTime(sequence, D, start_time)
        if checkTimesAvailable(sequence, D, classStartTimes, calendars):
            S.append(scheduleToReadable(sequence, classStartTimes))
    return S


def findPermutations(positions):
    no_pos_student, perm_template = processPositions(positions)
    perms = permutations(no_pos_student)
    return [stitch(p, perm_template) for p in perms]


def processPositions(positions):
    students = positions.keys()
    num_students = len(students)
    unknown_possition_students = []
    perm_template = [""] * num_students
    for s in students:
        pos = positions[s]
        if pos != 0:
            perm_template[pos-1] = s
        else:
            unknown_possition_students.append(s)
    return unknown_possition_students, perm_template


def stitch(permutation, perm_template):
    sequence = []
    empty_slots = 0
    for s in perm_template:
        if s:  # slot not empty
            sequence.append(s)
        else:
            sequence.append(permutation[empty_slots])
            empty_slots += 1
    return sequence


def sequenceToClassStartTime(sequence, D, start_time):
    times = [start_time]
    for i in range(len(sequence)):
        duration = D[sequence[i]]
        times.append(times[-1].plusPeriodNew(Period(duration)))
    return times


def checkTimesAvailable(sequence, D, startTimes, calendars):
    assert len(D) == len(startTimes)-1
    for i in range(len(D)):
        key = sequence[i]
        if "B" in key:
            continue  # ignore breaks
        if not calendars[key].checkTimeAvailable(startTimes[i], D[key]):
            return False
    return True


def scheduleToReadable(sequence, class_start_times):
    S = []
    for i in range(len(sequence)):
        class_start = class_start_times[i]
        class_end = class_start_times[i+1]

        key = sequence[i]

        if key[0] == "B":
            S.append("({:5})  {:20}  {} --> {}".format(key, "Break",
                                                       class_start, class_end))
            continue

        ID = int(key[1:])
        if key[0] == "S":
            student_name = get_object_or_404(Student, pk=ID).getName()[:20]
            S.append("({:5})  {:20}  {} --> {}".format(key, student_name,
                                                       class_start, class_end))
        elif key[0] == "C":
            chamber_group_name = get_object_or_404(ChamberGroup, pk=ID)\
                .name[:20]
            S.append("({:5})  {:20}  {} --> {}".format(key, chamber_group_name,
                                                       class_start, class_end))
    return "<pre>"+"<br/>".join(S)+"</pre>"
