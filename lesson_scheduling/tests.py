from django.test import TestCase
from django.urls import reverse

import testUtils as utils

# Create your tests here.
class IndexListViewTests(TestCase):
    URL = "lesson_scheduling:index"
    TEMPLATE = "lesson_scheduling/index.html"

    def test_no_teacher(self):
        """
        If no teacher exist, an appropriate message is displayed.
        """
        response = self.client.get(reverse(self.URL))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE)
        no_teacher_msg = "There are no teacher in the system."
        self.assertContains(response, no_teacher_msg, count=1)
        self.assertQuerysetEqual(response.context['teacher_list'], [])


    def test_past_question(self):
        """
        There should be a list of teachers ordered by their last name.
        """
        t1 = utils.createTeacher("Z", "Z")
        t2 = utils.createTeacher("A", "A")
        response = self.client.get(reverse(self.URL))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE)
        self.assertQuerysetEqual(
            response.context['teacher_list'],
            [repr(t2), repr(t1)]
        )
